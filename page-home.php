<?php
/*
 Template Name: Home Page
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div id="content" class="home">

				<?php $hero_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>

				<!-- <div id="hero" style="background: linear-gradient(to top, rgba(255,255,255,1),rgba(255,255,255,0) 40%), url(<?=$hero_image[0]?>);background-position:center;"> -->
				<div id="hero" style="background: url(<?=$hero_image[0]?>);background-position:center;background-attachment:fixed;">
					<img class="logo" src="<?php echo get_template_directory_uri(); ?>/library/images/logo.png">
					<h1 class="big-title">The Road to <span class="scripty">Transformation</span></h1>
					<p class="big-description">Sex trafficking is real.  <strong>Painfully real.</strong><br> But full restoration  by the power of Jesus Christ is <strong>just as real.</strong></p>


				</div>
				<h1 class="road-heading">This is the life of a trafficking victim.</h1>
				<div id="the-road">
				<div id="vulnerable" class="negative-heading left-column">
					<h2>Vulnerable</h2>
					<p>70% of trafficked and exploited young women have lived in at least one foster home.</p>
				</div>
				<img id="vulnerable-image" class="home-image negative-image right-column" src="<?php echo get_template_directory_uri(); ?>/library/images/sad-child.jpg" />
				<div style="clear:both;"></div>

				<div id="violated" class="negative-heading right-column">
					<h2>Violated</h2>	
					<p>Most woman who are sexually trafficked were sexually abused as a child.</p>
				</div>
				<img id="violated-image" class="home-image negative-image left-column" src="<?php echo get_template_directory_uri(); ?>/library/images/sad-child.jpg" />
				<div style="clear:both;"></div>

				<div id="victimized" class="negative-heading left-column">
					<h2>Victimized</h2>
					<p>The average age a woman enters the sex trafficking industry is 12-14 years old.</p>
				</div>
				<img id="victimized-image" class="home-image negative-image right-column" src="<?php echo get_template_directory_uri(); ?>/library/images/sad-child.jpg" />
				<div style="clear:both;"></div>

				<div id="controlled" class="negative-heading right-column">
					<h2>Controlled</h2>
					<p>Her pimp holds all her resources and tells her what to do, what to say, where to go, and how to act.</p>
				</div>
				<img id="controlled-image" class="home-image negative-image left-column" src="<?php echo get_template_directory_uri(); ?>/library/images/controlled.jpg" />
				<div style="clear:both;"></div>

				 </div> 

				<div id="chained" class="negative-heading">
					<h2>Chained</h2>
					<p>Filled with shame, pain, lies and terror.</p>
				</div>  
				<h1 class="road-heading">...and this is the road to transformation.</h1>
				<div id="the-road-transformation">
				<div id="set-free" class="positive-heading centered">
					<h2>Set Free</h2>
					<blockquote>You shall know the truth and the truth shall set you free. (John 8:32)</blockquote>
					<p>Once physical freedom is realized there is still much that must be set-free emotionally, mentally and spiritually. The web of lies must be unwoven through the threefold strategy of sound counseling, personal mentorship and caring discipleship.</p>
				</div> 

				<div id="healed" class="positive-heading">
					<h2>Healed</h2>
					<blockquote>He heals the broken in heart, and binds up their wounds. (Psalm 147:3)</blockquote>
					<p>Extreme physical, sexual and psychological abuse results in an inability to cope with past experiences and move forward in life. When she finds the safety and peace that comes from trusting God's loving care, a deep inner healing can take place. Through the compassionate support of others, she also begins learning how to care well for and bring restoration to her physical body through nutrition, exercise, creative expression and enjoyment of nature. </p>
				</div>

				<div id="renewed" class="positive-heading">
					<h2>Renewed </h2>
 					<blockquote>Do not be conformed to this world, but be transformed by the renewing of your mind. (Romans 12:2)</blockquote>
					<p>Instead of trying to go back to life before enslavement, she begins to realize that life can be more amazing than anything she imagined. She does not forget what has happened in the past, but she is no longer controlled by it. Hope and joy comes as she finds her satisfaction and approval in her Creator rather than in man. </p>
				</div>

				<div id="equipped" class="positive-heading">
					<h2>Equipped</h2>
					<blockquote>When wisdom enters your heart, And knowledge is pleasant to your soul, discretion will preserve you; Understanding will keep you, To deliver you from the way of evil. (Proverbs 2:10-12a)</blockquote>
					<p>Many key things have been missing as a result of her poor family life. This has been further exacerbated by being trapped in a world that rarely even allowed adequate rest, never mind giving space to grow in many practical life skills. Not only has she not graduated high school, but she never even started it. In addition to now filling in educational gaps, learning important life skills will keep her from sinking into poverty, returning to the sex industry or being taken advantage of in the future. </p>
				</div>

			</div>
							<div id="poured-out" class="positive-heading">
					<h2>Poured Out</h2>
					<!-- <blockquote>The Spirit of the Lord God is upon me; because the Lord hath anointed me to preach good tidings unto the meek, he hath sent me to bind up the brokenhearted, to proclaim liberty to the captives, and the opening of prison to them that are bound; (Isaiah 61:1-2)</blockquote> -->
					<p>Life now has vision and purpose. There is a suffering world in need of the very hope and joy that has been found. Life is no longer about personal survival, it is now about bringing the good news to others. </p>
				</div>


				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">




							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>


				</div>

			</div>


<?php get_footer(); ?>
